// Bài 1: Tính tiền lương nhân viên
/**
 * input: số ngày làm và lương cố định là 100.000
 * 
 * Xử lý: tính lương = số ngày làm việc * lương cố định
 * 
 * output: kết quả lương thu được với số ngày làm việc nhập vào
 */

var btnTinhLuong = document.getElementById("btnTinhLuong");
btnTinhLuong.onclick = function() {
    var inputSoNgayLuong = document.getElementById("inputSoNgayLuong").value * 1;
    var inputSoNgayLam = document.getElementById("inputSoNgayLam").value * 1;

    var Luong = document.getElementById("Luong");
    Luong.innerHTML = `Kết quả: ${inputSoNgayLam * inputSoNgayLuong} `;
}

// Bài 2: Tính giá trị trung bình
/**
 * input: nhập 5 số thực như ví dụ bên dưới
 * 
 * Xử lý: Cộng 5 số thực trước rồi chia 5 để lấy giá trị trung bình
 * 
 * output: Tính ra kết quả giá trị trung bình của 5 số thực đã nhập
 */
var btnTinhTrungBinh = document.getElementById("btnTinhTrungBinh");
btnTinhTrungBinh.onclick = function() {
    var inputSoThu1 = document.getElementById("inputSoThu1").value * 1;
    var inputSoThu2 = document.getElementById("inputSoThu2").value * 1;
    var inputSoThu3 = document.getElementById("inputSoThu3").value * 1;
    var inputSoThu4 = document.getElementById("inputSoThu4").value * 1;
    var inputSoThu5 = document.getElementById("inputSoThu5").value * 1;

    var trungBinh = document.getElementById("trungBinh");
    trungBinh.innerHTML = `Kết quả: ${(inputSoThu1 + inputSoThu2 + inputSoThu3 + inputSoThu4 + inputSoThu5) / 5} `;
}

// Bài 3: Quy đổi tiền
/**
 * input: Nhập số usd cần đổi và trị giá 23.500 1 usd sang vnd
 * 
 * Xử lý: lấy số usd cần đổi nhân với trị giá
 * 
 * output: Số tiền nhận được từ usd sang vnđ được xử lý ở công thức trên
 */

const USD_TO_VND = 23.500;
var btnQuyDoiTien = document.getElementById("btnQuyDoiTien");
btnQuyDoiTien.onclick = function() {
    var inputUSD = document.getElementById("inputUSD").value * 1;

    var quyDoiTien = document.getElementById("quyDoiTien");
    quyDoiTien.innerHTML =  "Kết quả: " + (inputUSD * USD_TO_VND).toLocaleString();
}

// Bài 4: Tính diện tích, chu vi hình chữ nhật
/**
 * input: nhập vào chiều dài, chiều rộng
 * 
 * xử lý: diện tích = dài * rộng, chu vi = (dài + rộng) *2
 * 
 * output: tính ra chu vi và diện tích từ chiều dài rộng nhập vào
 */

var btnTinh = document.getElementById("btnTinh");
btnTinh.onclick = function() {
    var inputDai = document.getElementById("inputDai").value * 1;
    var inputRong = document.getElementById("inputRong").value * 1;

    var ketQuaHCN = document.getElementById("ketQuaHCN");
    ketQuaHCN.innerHTML = `Kết quả: Diện tích = ${inputDai * inputRong}, Chu vi = ${(inputDai + inputRong) * 2} `
}

// Bài 5: Tính tổng 2 ký số
/**
 * input: nhập vào số có 2 ký số
 * 
 * xử lý: lấy số hàng chục bằng cách chia lấy phần nguyên, lấy số hàng đơn vị bằng cách chia lấy phần dư -> tính tổng 2 chữ số đã tách được
 * 
 * output: tổng 2 chữ số sau khi tách
 */

var btnTinhTong = document.getElementById("btnTinhTong");
btnTinhTong.onclick = function() {
    var input2so = document.getElementById("input2so").value * 1;

    var tinhTong =document.getElementById("tinhTong");
    tinhTong.innerHTML = `Kết quả: ${Math.floor(input2so / 10) + input2so % 10}`;
}
